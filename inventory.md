index  |  name              |  quantity  |  model                                             |  comments
-------|--------------------|------------|----------------------------------------------------|------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
8      |  flash             |  24        |                                                    |
14     |  pink              |  17        |  pcengines-alix.2d2 LX800 256mb 2lan 2minicpi usb  |  slots for mini pci and lan cards. url-> titanwirelessonline.com/ALIX-256MB-2LAN-2mPCI-2USB-p/lx-800-2u.htm
10     |  grayson           |  19        |  wmia-199n/eu                                      |  wireless card with linux support. Can possibly use for galileo.url->http://www.sparklan.com/p2-products-detail.
1      |  black             |  6         |  Qcom QCM-Q802xKG                                  |  miniPCIe wifi card. Fits in Galileo. Could not get to work.
7      |  damian            |  4         |  dnma-92                                           |  miniPCI wifi card. Specifically for directly dual band
16     |  tim               |  4         |  wlm200nx                                          |  miniPCI radio card
4      |  by                |  2         |  pcengines--alix3d2/lx800                          |  Basically mini pink
11     |  jason             |  2         |  wlm54agp23                                        |  miniPCI.Wifi card. Linux driver.
5      |  choc              |  1         |  samsung--pc3200u-30331-b2                         |  RAM
6      |  cisco_lan         |  1         |                                                    |
3      |  bluetooth-dongle  |  7         |                                                    |
9      |  gordon            |  1         |  c0-e2-22aa--00-21-4a-63-7b-e6                     |  -
12     |  late              |  1         |  samsung--pc3200u-30331-z                          |  RAM DDR
13     |  mouse             |  1         |                                                    |
2      |  blue              |  1         |  MIB510CA                                          |  Serial Interface Board. Programming interface. RS-232 Serial Gateway. MICA2, MICA2DOT Connectivity.
15     |  sean              |  1         |  nl-3054mparies                                    |  miniPCI wifi card
17     |  wade              |  1         |  wmia-199n                                         |  wireless card with linux support. Can possibly use for galileo.url->http://www.sparklan.com/p2-products-detail.php?PKey=3d2c2AkR9TpemVcnwaj2vdsN5epuoVNF1q7BOFoaNQ&WMIA-199N
18     |  wireless_lan_usb  |  1         |                                                    |
19     |  yellow            |  1         |  St*rgate                                          |  Don't know
20     |  roger             |  1         |  bigguy-littleguy                                  |  Don't know
21     |  crossbow          |  6         |  MICAz                                             |  2.4 GHz wireless measurement system. Designed specifically for deeply embedded Sensor networks. 205kbps High Data Rate Radio. Wireless Communications with every node as router capability. Expansion Connecter for Light, Temperature, RH, Barometric Pressure, Accelaration/Seismic, Acoustic, Magnetic and other MEMSIC Boards. Used for Indoor Building Monitoring and Security. Acoustic, Video, Vibration and Other High Speed Sensor Data. Large Scale Sensor Networks. url->http://www.memsic.com/userfiles/files/Datasheets/WSN/micaz_datasheet-t.pdf  
22     |  tmote             |  8         |   Tmote Sky                                        |  IEEE 802.15.4 Wireless Transceiver. 8Mhz Microcontroller. Humidity, Temperature and Light Sensors included. TinyOS Support: Mesh Networking and Communication Implementation. Programming and Data Collection via USB. url-> http://www.eecs.harvard.edu/~konrad/projects/shimmer/references/tmote-sky-datasheet.pdf
23     |  crossbow batt     |  1         |   Crossbow battery Board                           |  Don't know
25     |  TI Sniffer        |  3         |   TI Sniffer CC2540                                |  TI BLE Packet Sniffer
26     |  Intel Galileo     |  2+        |   Intel Galileo                                    |  Intel Galileo
27     |  BLED112           |  1         |  Bluegiga BLED112                                  |  Programmable with python. Can be used with Galileo.
28     |  nRFDongle         |  1         |  nRF Dongle                                        |  Programmable. Can be used as packet sniffer.
29     |  nRF51 Board       |  2+        |  nRF51DK                                           |  Programmable board
30     |  Battery Blue      |  12        |  Battery                                           |  Battery
31     |  Battery Black     |  7         |  Battery                                           |  Battery. All in lab accounted for except the one with a yellow post it note on top.
32     |  Battery Grey      |  12        |  Battery                                           |  Battery
33     |  Soekris Engineerin|  2         |  Sockeris Engineering                              |  Don't know.
34     |  Arduino UNO       |  1         |  Arduino UNO                                       |  Arduino UNO
35     |  Funduino Nano     |  2         |  Funduino Nano                                     |  Funduino Nano
36     |  XBee              |  2         |  XBee                                              |  XBee
37     |  XBee Pro          |  2         |  XBee Pro                                          |  XBee Pro
38     |  GPS Receiver      |  11        |  GPS                                               |  GPS
39     |  RS-232 to RS-485  |  1         |  {}                                                |  ~{''}~
40     |  Multi Hub         |  2         |                                                    |
41     |  URI Beacon        |  23+       |                                                    |  
42     |  Sensoro Smart Beacon | 1+      |                                                    |
